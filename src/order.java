import java.util.Scanner;

public class order {
    public static void orderfood(String food){
        System.out.print("You have ordered "+food+". ");
        System.out.println("Thank you!");
    }
    public static void main(String[] args) {

        System.out.print("What would you lke to order:\n1. Tempura\n2. Ramen\n3. Udon\nYour order[1-3]:");

        Scanner userIn = new Scanner(System.in);
        int choice = userIn.nextInt();
        userIn.close();

        if(choice==1){
            orderfood("Tempura");
        }

        else if(choice==2){
            orderfood("Ramen");
        }

        else if(choice==3){
            orderfood("Udon");
        }

    }
}