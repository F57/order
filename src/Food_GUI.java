import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Food_GUI {
    private JPanel root;
    private JButton udonButton;
    private JButton ramenButton;
    private JButton tempuraButton1;
    private JTextArea textbox;

    public void orderfood(String food){
        int confirming = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order Tempura?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirming == 0) {
            System.out.println("Order for " + food + " received");
            JOptionPane.showMessageDialog(null, "Order for "+food+" received");
        }
    }

    public Food_GUI() {

        tempuraButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("Ramen");
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderfood("Udon");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Food_GUI");
        frame.setContentPane(new Food_GUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
